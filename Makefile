setup:
	cp .env.example .env
	docker-compose pull
	docker-compose build
	@if [ -z $$VIRTUAL_ENV ]; then \
		pip install --user pre-commit; \
	else \
		pip install pre-commit; \
	fi

superuser:
	docker exec -it cadastro_processos python manage.py createsuperuser

migrate:
	docker exec -it cadastro_processos python manage.py migrate
	docker-compose stop web
	docker-compose up web

run:
	docker-compose up web

disconnect:
	docker-compose down
