## Instalação

Este projeto requer python>=3.6, docker e docker-compose.

```
$ make setup
```

## Executando o projeto

Um arquivo `.env` na raíz do projeto foi criado com as variáveis de ambiente necessárias (exemplos são encontrados no arquivo `.env.example`). Caso deseje mudar algum comportamento do projeto, modifique o `.env` antes de executar algum comando.

Se desejar que as planilhas sejam persistidas no AWS S3, atualize a variável `S3_STORAGE_ENABLED` para `True` e as variáveis `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` e `S3_STORAGE_BUCKET_NAME` para as respectivas chaves da conta e nome do bucket.

### Para iniciar o servidor:

```
$ make run
```

### Para iniciar o banco de dados:

```
$ make migrate
```

### Para criar um superuser capaz de mexer na página de admin:

```
$ make superuser
```

Abra o endereço `http://127.0.0.1:8000` no navegador.

Voilà!

### Caso queira derrubar o serviço:

```
$ make disconnect
```

## Caso tenha problema de permissão:

Execute no shell no seu computador o comando

```
$ id -u
```

Coloque o resultado na variável `UID` no `.env`. Execute `make disconnect`, `docker-compose build` e `make run` novamente.
