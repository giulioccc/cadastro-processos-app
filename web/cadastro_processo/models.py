from django.core.validators import FileExtensionValidator, RegexValidator
from django.db import models


def whitespace_to_underline(text):
    return text.replace(" ", "_")


def planilha_filename(instance, filename):
    cliente_sem_whitespace = whitespace_to_underline(str(instance.cliente))
    nome_sem_whitespace = whitespace_to_underline(str(instance.nome))
    return f"planilhas/{cliente_sem_whitespace}/{nome_sem_whitespace}.csv"


class CadastroProcesso(models.Model):
    nome = models.CharField(
        max_length=200, unique=True, validators=[RegexValidator(regex="^[ a-zA-Z]+$")]
    )
    cliente = models.CharField(
        max_length=200, validators=[RegexValidator(regex="^[ a-zA-Z]+$")]
    )
    arquivo = models.FileField(
        upload_to=planilha_filename,
        validators=[FileExtensionValidator(allowed_extensions=["csv"])],
    )

    def __str__(self):
        return self.nome
