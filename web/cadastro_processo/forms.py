from django import forms

from .models import CadastroProcesso


class CadastroProcessoForm(forms.ModelForm):
    class Meta:
        model = CadastroProcesso
        fields = ("nome", "cliente", "arquivo")
