from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView
from import_export import resources
from tablib import Dataset

from processo.models import Processo

from .forms import CadastroProcessoForm
from .models import CadastroProcesso


class CadastroProcessoView(SuccessMessageMixin, CreateView):
    model = CadastroProcesso
    form_class = CadastroProcessoForm
    template_name = "cadastro_processo.html"
    success_url = reverse_lazy("cadastro_processo")
    success_message = "Planilha %(nome)s cadastrada com sucesso"

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)

        if form.is_valid():
            planilha = form.files["arquivo"]
            headers = planilha.readline().decode().strip().split(";")
            for line in planilha.readlines():
                dados = Dataset(line.decode().strip().split(";"), headers=headers)
                processo_resource = resources.modelresource_factory(model=Processo)()
                result = processo_resource.import_data(dados, dry_run=True)
                if not result.has_errors():
                    processo_resource.import_data(dados)

            planilha.seek(0)

        return super().post(request, *args, **kwargs)

    def get_success_message(self, cleaned_data):
        return self.success_message % dict(cleaned_data, nome=self.object.nome)
