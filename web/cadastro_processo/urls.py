from django.urls import path

from .views import CadastroProcessoView

urlpatterns = [path("", CadastroProcessoView.as_view(), name="cadastro_processo")]
