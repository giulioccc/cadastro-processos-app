from django.core.validators import MinLengthValidator, RegexValidator
from django.db import models


class Processo(models.Model):
    pasta = models.CharField(
        max_length=5, validators=[MinLengthValidator(5), RegexValidator(regex="^\d+$")]
    )
    comarca = models.CharField(
        max_length=200, validators=[RegexValidator(regex="^[ a-zA-Z]+$")]
    )
    uf = models.CharField(
        max_length=2,
        validators=[MinLengthValidator(2), RegexValidator(regex="^[A-Z]+$")],
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["pasta", "comarca", "uf"], name="chave_processo"
            )
        ]

    def __str__(self):
        return self.pasta
